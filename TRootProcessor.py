##########################################################
#   Author: Alkaid Cheng       
#   Email : ccheng84@wisc.edu
#   Date  : Jul 2019
##########################################################
import ROOT
import numpy as np
import os
from pdb import set_trace
from ROOT import gSystem
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    '-i','--input', action='store', help='data input: root file, globs, npz or npy file containing list of root files or globs', required=True)
parser.add_argument(
    '-d','--datacard', action='store', help='data card describing the actions to be performed on the root files',required=True)
parser.add_argument(
    '-e', '--extension', action='store',default='', help='npz file with the same keys as the input to export external information to data card',required = False)  
parser.add_argument(
    '-l', '--log', action='store',default='', help='name of the log file where finished keys will be saved',required = False)  
parser.add_argument(
    '--delphes', action='store',default='', help='path to the Delphes library',required = False)  
         
args = parser.parse_args()

def main(args):
    # Disable root display
    ROOT.gROOT.SetBatch(ROOT.kTRUE)
    # Load shared libraries
    script_basedir = os.path.dirname(__file__)
    gSystem.Load(os.path.join(script_basedir,"macros/src/TRootProcessor_cc"))
    ROOT.Alkaid.TRootProcessor(args.input, args.datacard, args.log, args.extension, args.delphes)

if __name__ == '__main__':
    main(args)
