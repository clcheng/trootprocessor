// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019

#include "inc/TRootProcessor.h"

using namespace ROOT::VecOps;

namespace Alkaid{
	
TRootProcessor::TRootProcessor(const std::string &input, const std::string &datacard,
        const std::string &logfile, const std::string &extension, const std::string &delphespath){
	LoadCustomFunction();
	LoadDelphesLib(delphespath);
	StartOver(input, datacard, logfile, extension);
}

bool TRootProcessor::ResetAll(){
    this->tree_name = "";
    Type::Dict{}.swap(input_files);
    Type::NestedDict{}.swap(extension_data);
    std::vector<Type::StrPair>().swap(actions_raw);
    std::vector<std::vector<std::string>>().swap(actions_validated);
    ResetSingle();
    return true;
}

bool TRootProcessor::ResetSingle(){
	std::vector<std::vector<std::string>>().swap(actions_extended);
    return true;
}

bool TRootProcessor::StartOver(const std::string &input, const std::string &datacard,
        const std::string &logfile, const std::string &extension){
	SetLogFileName(logfile);
	if ((logfile != "")&&FileManager::IsRegularFile(logfile))
		finished_keys = Utility::GetLinesFromFile(logfile);
	if (!(LoadInputFiles(input)&&LoadDataCard(datacard)&&LoadExtension(extension)))
		return false;
	return RunAll();
}

bool TRootProcessor::LoadCustomFunction(){
	Alkaid::CustomFunction::DeclareAllCustomFunctions();
	return true;
}

bool TRootProcessor::LoadInputFiles(const std::string & input){
	this->input_files = Alkaid::Utility::GetFileNamesDict(input,"root");
	return (this->input_files.size() > 0);
}

bool TRootProcessor::LoadDelphesLib(const std::string & delphespath){
	if (delphespath == "")
		return false;
	//gInterpreter->AddIncludePath(delphespath.c_str());
	//gInterpreter->AddIncludePath((delphespath+"/external").c_str());
	gSystem->Load((delphespath+"/libDelphes").c_str());
	return true;
}

bool TRootProcessor::LoadDataCard(const std::string & datacard){

	auto datinfo_raw = Utility::GetStrPairsFromFile(datacard, kActionList);
	if (datinfo_raw.size() == 0)
		return Utility::PrintErrorMessage("ERROR: No action found in datacard!");

	auto datinfo_map = Utility::GetMappedStrFromFile(datinfo_raw);

	// Search for definition of tree name in datacard and push to memory
	if (datinfo_map.count(kTreeName) == 0)
		return Utility::PrintErrorMessage("ERROR: No definition of tree name found in datacard!");
	else if (datinfo_map[kTreeName].size() > 1)
		return Utility::PrintErrorMessage("ERROR: More than one definition of tree name found in datacard!");
	else
		SetTreeName(datinfo_map[kTreeName][0]);

	this->actions_raw = datinfo_raw;

	ReplaceSubscriptOperator(datinfo_raw);
	RemoveNewLine(datinfo_raw);	

	std::vector<std::vector<std::string>> datinfo_validated;

	if (!FormatActions(datinfo_raw,datinfo_validated))
		return false;

	if (!ValidateDataCard(datinfo_validated))
		return false;

	this->actions_validated = datinfo_validated;

	std::cout<<"Successfully loaded datacard "<<datacard<<"\n";

	return true;
}

bool TRootProcessor::LoadExtension(const std::string &extension){
	if (extension == "")
		return Alkaid::Utility::PrintMessage("INFO: No extension found. Skipped.\n",true);
	extension_data = Alkaid::Utility::NestedNpzToCMap(extension);

	return Alkaid::Utility::PrintMessage("Successfully loaded extension.\n",extension_data.size() > 0);
}

std::vector<Type::StrPair> &TRootProcessor::ReplaceSubscriptOperator(std::vector<Type::StrPair> &target){
    for (auto &s : target)
        s.second = std::regex_replace(s.second,RegEx::kSubscript,"$1.at($2)");
    return target;
}

std::vector<Type::StrPair> &TRootProcessor::RemoveNewLine(std::vector<Type::StrPair> &target){
    for (auto &s : target)
        s.second = Utility::StrRemoveAll(s.second,'\n');
    return target;
}

bool TRootProcessor::Format_SaveAsRoot(const Type::StrPair& data, 
	std::vector<std::vector<std::string>>& target){

	auto match1 = Utility::GetMatchedStr(data.second,RegEx::kDefineListEx);
	auto match2 = Utility::GetMatchedStr(data.second,RegEx::kAliasEx);
	
	if ((match1.size()!=3)||(match2.size()!=2))
		return false;
	if	((match1[0][1]!="Frame")||(match1[1][1]!="FileName")||
		(match1[2][1]!="Variables"))
		return false;
	if	((match2[0][1]!="TreeName")||(match2[1][1]!="ToNumpy"))
		return false;	

	target.emplace_back(std::vector<std::string>{kSaveAsRoot,
		match1[0][1],match1[0][2],match2[0][1],match2[0][2],
		match1[1][1],match1[1][2],match1[2][1],match1[2][2],
		match2[1][1],match2[1][2]});

	return true;
}

bool TRootProcessor::Format_Define(const Type::StrPair& data, 
	std::vector<std::vector<std::string>>& target){	

	auto match = Utility::GetMatchedStr(data.second,RegEx::kLeadingEquationEx);
	if (match.size() != 1)
		return false;
	
	target.emplace_back(std::vector<std::string>{kDefine,match[0][1],match[0][2]});
	return true;
}

bool TRootProcessor::Format_Alias(const Type::StrPair& data, 
	std::vector<std::vector<std::string>>& target){

	auto match = Utility::GetMatchedStr(data.second,RegEx::kAliasOnlyEx);
	if (match.size() != 1)
		return false;

	target.emplace_back(std::vector<std::string>{kAlias,match[0][1],match[0][2]}); 
	return true;
}

bool TRootProcessor::Format_SaveOrLoadFrame(const Type::StrPair& data, 
	std::vector<std::vector<std::string>>& target){

	if (!Utility::IsMatchRegEx(data.second,RegEx::kVariableOnlyEx))
		return false;
	target.emplace_back(std::vector<std::string>{data.first,data.second});  
	return true;
}

bool TRootProcessor::Format_General(const Type::StrPair& data, 
	std::vector<std::vector<std::string>>& target){
	target.emplace_back(std::vector<std::string>{data.first,data.second});
	return true;
}


bool TRootProcessor::FormatActions(const std::vector<Type::StrPair> &source,
 										   std::vector<std::vector<std::string>> &target){

	std::vector<std::vector<std::string>> buffer;

    for (auto &s : source)
    	if (format_action_map.count(s.first) != 0){
    		if (!(this->*format_action_map.at(s.first))(s,buffer))
				return Utility::PrintErrorMessage(
	    		"ERROR: Invalid format for "+s.first+" in datacard!"); 
    	}

    //Remove leading and trailing whitespace, remove redundant whitespace
    Utility::VStrClean(buffer);

    target = buffer;
    return true;
}

bool TRootProcessor::ExtendDataCard(const std::string &key){
	if (actions_validated.size() == 0)
		return false;
	std::vector<std::vector<std::string>> result = actions_validated;
	std::vector<std::string> extendedvar;
	for (auto &action: result)
		for (auto &element: action){
			extendedvar = Utility::GetMatchedSubStr(element,RegEx::kMoney,1);
			Utility::VectorSortRemoveDuplicates(extendedvar);
			if (extendedvar.size() != 0)
				for (auto &var: extendedvar){
					if (extension_data[key].count(var) == 0)
						return Utility::PrintErrorMessage("ERROR: "
							"Unable to extend the variable content of $"+var+"$");
					element = Utility::StrReplaceAll(element,
						"$"+var+"$",extension_data[key][var][0]);
				}
			
		}
	this->actions_extended = result;
	return true;
}

bool TRootProcessor::ValidateDataCard(std::vector<std::vector<std::string>> &target){

    for (auto &s : target)
    	if (((s[0] == kDefine)&&(!Validate_Define(s)))||
    		((s[0] == kFilter)&&(!Validate_Filter(s))))
    		return Utility::PrintErrorMessage("ERROR: Failed to validate datacard!");
	return true;
}


bool TRootProcessor::Validate_Filter(std::vector<std::string> &s){
	s[1] = std::regex_replace(s[1],RegEx::kBraces,"$1");
	return true;
}

bool TRootProcessor::Validate_Define(std::vector<std::string> &s){

	//Remove double braces for custom definition
	if (Utility::IsMatchRegEx(s[2],RegEx::kDoubleBraces))
		s[2] = std::regex_replace(s[2],RegEx::kDoubleBraces,"$1");
	//Format the definition with a return statement
	else
		s[2] = (std::string) "return "+s[2]+";";

	return true;
}


bool TRootProcessor::RunAll(){
	
	ROOT::EnableImplicitMT();
	ROOT::EnableThreadSafety();
	//Check all necessary information has been initialized
	if ((tree_name == "")||(input_files.size() == 0)||(actions_validated.size() == 0))
		return Utility::PrintErrorMessage("ERROR: Cannot initialize"
			"dataframe. Please check that:\n1. At least one root file was included,\n"
			"2. Tree name was set,\n3. Data card was loaded.");

	if ((extension_data.size() !=0) &&(!Utility::MapsHaveSameKeys(input_files,extension_data)))
		return Utility::PrintErrorMessage("ERROR: Extension data should have the same key(s) as the input files");	

	for (auto &input: input_files)
		if (!RunSingle(input.first))
			return false;

	return true;
}

bool TRootProcessor::RunSingle(const std::string &key){
	if (input_files.count(key) == 0)
		return Utility::PrintErrorMessage("ERROR: Unknown output key: \""+key+"\".");

	this->ResetSingle();

	// Skip jobs that have already been processed
	if (Utility::VectorContains(finished_keys,key))
		return Utility::PrintMessage("INFO: Output key \""+key+
			"\" already finished. Will be skipped.\n",true);

	std::cout<<"INFO: Started a new run on the output key: \""<<key<<"\"\n";

	std::map<std::string,TDataFrameWrapper> frames =
	    { {kDefaultFrame, TDataFrameWrapper(ROOT::RDataFrame(tree_name,input_files[key]))}};
	
    std::vector<SnapRet_t> snapshots;
	std::vector<SaveInfo> numpy_saveinfo;

	if (!ExtendDataCard(key))
		return false;

	for (auto &action : actions_extended){
		if (!(((action[0] == kDefine)&&Action_Define(action,frames))||
		   ((action[0] == kFilter)&&Action_Filter(action,frames))||
		   ((action[0] == kAlias)&&Action_Alias(action,frames))||
		   ((action[0] == kSaveFrame)&&Action_SaveFrame(action,frames))||
		   ((action[0] == kLoadFrame)&&Action_LoadFrame(action,frames))||
		   ((action[0] == kSaveAsRoot)&&Action_SaveAsRoot(action,key,frames,snapshots, numpy_saveinfo))||
		   (action[0] == kTreeName)))
		return Utility::PrintErrorMessage("Unknown error occurred while processing "
			"action. Aborted.");
	}

	std::map<std::string, ROOT::RDF::RResultPtr<ROOT::RDF::RCutFlowReport>> reports;
	for (auto &frame: frames)
		reports[frame.first] = frame.second.Report();

	TStopwatch stopwatch;
	std::cout<<"#################################################################################################\n";
	std::cout<<"INFO: Start Processing...\n";
	std::cout<<"#################################################################################################\n";
	stopwatch.Start();
	for (auto& report: reports){
		std::cout<<"INFO: Filtering statistics for the frame \""+report.first+"\":\n";
		report.second.GetPtr()->Print();
		std::cout<<"#################################################################################################\n";
	}

	stopwatch.Stop();

	Utility::PrintStopwatchTime(stopwatch);

	for (auto &info : numpy_saveinfo)
		Action_SaveAsNumpy(info.filename,info.treename);

	if (log_filename != "")
		FileManager::WriteTextToLogFile(log_filename,key);

	return true;
}


bool TRootProcessor::Action_Define(const std::vector<std::string> &actioninfo,
	std::map<std::string,TDataFrameWrapper>& frames){
	std::cout<<"INFO: Defined new variable\n\tVariable Name: "<<actioninfo[1]<<"\n"
	"\tDefinition: "<<actioninfo[2]<<"\n\n";
	frames[kDefaultFrame].Define(actioninfo[1],actioninfo[2]);
	return true;
}
bool TRootProcessor::Action_Filter(const std::vector<std::string> &actioninfo,
	std::map<std::string,TDataFrameWrapper>& frames){
	std::cout<<"INFO: Created new selection:\n\t"<<actioninfo[1]<<"\n\n";
	frames[kDefaultFrame].Filter(actioninfo[1], actioninfo[1]);
	return true;
}

bool TRootProcessor::Action_Alias(const std::vector<std::string> &actioninfo,
	std::map<std::string,TDataFrameWrapper>& frames){
	std::cout<<"INFO: Defined new alias\n\tAlias Name: "<<actioninfo[1]<<"\n"
	"\tVariable Name: "<<actioninfo[2]<<"\n\n";
	frames[kDefaultFrame].Alias(actioninfo[1],actioninfo[2]);
	return true;
}

bool TRootProcessor::Action_SaveFrame(const std::vector<std::string> &actioninfo,
	std::map<std::string,TDataFrameWrapper>& frames){
	std::cout<<"INFO: Saved new frame: "<<actioninfo[1]<<"\n\n";
	frames[actioninfo[1]] = frames[kDefaultFrame];
	return true;
}

bool TRootProcessor::Action_LoadFrame(const std::vector<std::string> &actioninfo,
	std::map<std::string,TDataFrameWrapper>& frames){
	if (frames.count(actioninfo[1]) == 0)
		return Utility::PrintErrorMessage("ERROR: "
			"Failed to load unknown frame \""+actioninfo[1]+"\". Aborted.");
	std::cout<<"INFO: Loaded the frame: "<<actioninfo[1]<<"\n\n";
	frames[kDefaultFrame] = frames[actioninfo[1]];
	return true;
}

bool TRootProcessor::Action_SaveAsRoot(const std::vector<std::string> &actioninfo, const std::string &key,
	std::map<std::string,TDataFrameWrapper>& frames, std::vector<SnapRet_t> &snapshots,
	std::vector<SaveInfo> &numpy_saveinfo){

	std::vector<std::string> save_frames = Utility::StrSplit(actioninfo[2],",");
	Utility::VStrClean(save_frames);

	//Check all frames are defined
	for (auto &frame: save_frames)
		if (frames.count(frame) == 0)
			return Utility::PrintErrorMessage("ERROR:"
				" Unknown frame \""+frame+"\" to save. Aborted.\n");

	std::string treename = actioninfo[4];

	std::vector<std::string> save_filenames = Utility::StrSplit(actioninfo[6],",");
	Utility::VStrClean(save_filenames);

	//Check each frame has a corresponding save file name 
	if (save_frames.size() != save_filenames.size())
		return Utility::PrintErrorMessage("ERROR: In saving root/numpy files"
		", the number of frames should match the number of file names.\n\tFrames to save: "+
		actioninfo[2]+"\n\tFile names captured: "+actioninfo[6]+"\n");

	
	std::vector<std::string> save_columns = Utility::StrSplit(actioninfo[8],",");
	Utility::VStrClean(save_columns);

	bool save_all = Utility::VectorContains(save_columns,(std::string)"All");

	//Check all variables to be saved are defined
	if (!save_all)
		for (UInt_t i = 0 ; i < save_frames.size(); i++){
			std::vector<std::string> valid_columns = frames[save_frames[i]].GetColumnNames();
			if (!Utility::VectorIsSubset(valid_columns,save_columns)){
				auto undefined_columns = Utility::VectorToStr(
					Utility::VectorDifference(save_columns,valid_columns));
				return Utility::PrintErrorMessage("ERROR:"
					" Undefined variables \""+undefined_columns+"\" canno be saved. Aborted.\n");
			}	
		}

	std::string save_numpy = actioninfo[10];

	for (UInt_t i = 0 ; i < save_frames.size(); i++){
		std::string save_filename = key + save_filenames[i];
		std::string print_var_names = save_all? "All" : Utility::VectorToStr(save_columns);
		std::cout<<"INFO: Booked Action for Saving Root File(s):"
		"\n\tFrame: "<<save_frames[i]<<"\n\tFile Name: "<<save_filename<<""
		"\n\tTree Name: "<<treename<<"\n\tVariables: "<<print_var_names<<"\n\n";
		FileManager::MakeDirsForFile(save_filename);
		if (!save_all)
			snapshots.emplace_back(frames[save_frames[i]].Snapshot(treename,save_filename,save_columns, true));
		else
			snapshots.emplace_back(frames[save_frames[i]].Snapshot(treename,save_filename));
		if ((save_numpy == "True") || (save_numpy == "TRUE"))
			numpy_saveinfo.emplace_back(save_filename,treename);
	}

	return true;
}

bool TRootProcessor::Action_SaveAsNumpy(const std::string filename, const std::string treename){
    return CPy::GetPyResultHere<bool>("CPy","RootToNumpy",treename+"\",\""+filename);
}

} //namespace Alkaid