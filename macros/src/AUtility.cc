// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019

#include "inc/AUtility.h"

namespace Alkaid{
namespace Utility{

std::string StrTrim(const std::string& str,
                 const std::string& whitespace){
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

std::string StrReduce(const std::string& str,
                   const std::string& fill,
                   const std::string& whitespace){
    // trim first
    auto result = StrTrim(str, whitespace);

    // replace sub ranges
    auto beginSpace = result.find_first_of(whitespace);
    while (beginSpace != std::string::npos)
    {
        const auto endSpace = result.find_first_not_of(whitespace, beginSpace);
        const auto range = endSpace - beginSpace;

        result.replace(beginSpace, range, fill);

        const auto newStart = beginSpace + fill.length();
        beginSpace = result.find_first_of(whitespace, newStart);
    }

    return result;
}

std::string StrClean(const std::string &s, const int &option){
    if (option & kClean)
        return StrReduce(StrRemoveAll(s,'\r'));
    std::string ss = s;
    if (option & kRemoveCarrierReturn)
        ss = StrRemoveAll(ss,'\r');
    if (option & kTrimWhitespace)
        ss = StrTrim(ss);
    if (option & kReduceWhitespace)
        ss = StrReduce(ss);
    return ss;
}

std::vector<std::string> StrSplit(const std::string &str, const std::string &delim){
    std::vector<std::string> result;
    TIter iObj(((TString)str).Tokenize(delim));
    while (TObject * obj = iObj())
        result.push_back((std::string)((TObjString*)obj)->String());
    return result;
}

void VStrClean(std::vector<std::string>&vstr, const int &option){
    for (auto &s : vstr)
        s = StrClean(s,option);
}

void VStrClean(std::vector<std::vector<std::string>>&vstr, const int &option){
    for (auto &v: vstr)
        VStrClean(v,option);
}

bool IsMatchRegEx(const std::string &s, const std::regex &re){
    std::smatch match;
    return std::regex_match(s,match,re);
}

std::vector<std::string> GetMatchedSubStr(const std::string &s, const std::regex &re, const size_t pos){
    std::vector<std::string> result;
    std::smatch match;
    for( std::sregex_iterator it(s.begin(), s.end(), re), it_end; it != it_end; ++it ){
        match = *it;
        result.emplace_back(match.str(pos));
    }
    return result;
}

std::vector<std::vector<std::string>> GetMatchedStr(const std::string &s, const std::regex &re){
    std::vector<std::vector<std::string>> result;
    std::smatch match;
    for( std::sregex_iterator it(s.begin(), s.end(), re), it_end; it != it_end; ++it ){
        std::vector<std::string> sub_result;
        match = *it;
        for (auto &s: match)
            sub_result.emplace_back(s);
        result.emplace_back(sub_result);
    }
    return result;
}


std::vector<std::string> GetLinesFromFile(
    const std::string &inFile, const std::string &commentStr,
    const int &option, const bool rmEmpty){

    std::vector<std::string> result;
    std::ifstream ifs(inFile.c_str());
    if (ifs){
        std::string line;
        while (std::getline(ifs, line)){
            TrimAfterSubStr(line,commentStr);
            if (rmEmpty && (StrIsBlank(line)))
                continue;
            result.emplace_back(StrClean(line,option));
        }
    }
    return result;
}

std::vector<Type::StrPair> GetStrPairsFromFile(
    const std::string &inFile, const std::vector<std::string> keys,
    const std::string &separator, const Type::StrPair &braces,
    const std::string &commentStr, const int &option){

    auto Fill = [](std::vector<Type::StrPair> &vec,
        std::string &key, std::string &value, const int &option){
        key = StrClean(key,option), value = StrClean(value,option);
        vec.emplace_back(Type::StrPair(key,value));
        key = "", value = "";
    };

    std::vector<Type::StrPair> result;
    const std::vector<std::string> lines = GetLinesFromFile(inFile, commentStr, option);
    size_t pos;
    std::string key = "";
    std::string value = "";
    bool inside_brace = false;

    for (auto &line : lines){
        if (inside_brace){
            //Search for the end brace and push accumulated contents to the result
            if ((pos = line.find(braces.second)) != std::string::npos){
                value += line.substr(0,pos+size(braces.second));
                Fill(result,key,value,option);
                inside_brace = false;
            }
            else
                value += (line+"\n");
            continue;
        }
        
        if ((pos = line.find_first_of(separator))!=std::string::npos){
            //Skip empty key
            if (pos==0)
                continue;            
            key = line.substr(0,pos);

            //Filter unwanted key
            if ((keys.size()!=0)&&(!VectorContains(keys, key))){
                std::cerr<<"WARNING: Unknown key name: "<<key<<"\n. Skipped";
                continue;   
            }

            std::string content = line.substr(pos+1);
            //Skip empty content
            if (StrIsBlank(content))
                continue;
            //Search for the start brace
            if (content.find(braces.first)!=std::string::npos){
                size_t buf_pos = content.find(braces.second);
                //Push to result if end brace is in the same line
                if (buf_pos != std::string::npos)
                   Fill(result,key,value = content.substr(buf_pos+size(braces.second)-1),option);
                //Continue to search for contents inside brace until reaches the end brace
                else{ 
                    inside_brace = true;
                    value += content;
                }
            }
            else
                Fill(result,key,content,option);
        }
    
    }
    return result;
}

Type::Dict GetMappedStrFromFile(
    const std::string &inFile, const std::vector<std::string> keys,
    const std::string &separator, const Type::StrPair &braces,
    const std::string &commentStr,const int &option){

   return GetMappedStrFromFile(GetStrPairsFromFile(inFile, keys, separator, braces, commentStr, option));
}

Type::Dict GetMappedStrFromFile(
    const std::vector<Type::StrPair>& str_pairs){

    Type::Dict result;
    for (auto &data : str_pairs)
        AddElementToVectorMap(result,data.first,data.second);
    return result;
}



std::vector<std::string> GetVarNamesInStr(const std::string &s){
    //Remove the member in a variable, i.e. variable.member -> variable
    std::string ss = std::regex_replace(s,RegEx::kMember,"$1");
    //Remove function names
    ss = std::regex_replace(ss,RegEx::kFunctionName,"");
    std::vector<std::string> result = Flatten2DVector(GetMatchedStr(ss,RegEx::kVariable));
    return VectorSortRemoveDuplicates(result);
}

std::vector<std::string> GetVarNamesInStr(const std::vector<std::string> &sVec){
    std::vector<std::string> result;
    for (auto &s : sVec)
        VectorBackInsert(result,GetVarNamesInStr(s));
    return VectorSortRemoveDuplicates(result);
}

} // namespace Utility
} // namespace Alkaid