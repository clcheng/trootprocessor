// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019
#ifndef _ACPy_
#define _ACPy_

// system include(s)
#include<string>
#include<algorithm>

// ROOT include(s)
#include "TROOT.h"
#include "TPython.h"

namespace Alkaid{
namespace CPy{

    inline std::string GetCurrentDirectory() {
        std::string f = __FILE__;
        const size_t pos = f.find_last_of("\\/");
        return (std::string::npos == pos) ? "" : f.substr(0, pos);
    }

    inline bool PyModuleExists(const std::string& module, const std::string& path = ""){
        std::string expression;
        if (path != ""){
            TPython::Exec("import sys");
            expression = "sys.path.insert(1,\""+path+"\")";
            TPython::Exec(expression.c_str());
        }
        TPython::Exec("from importlib import util");
        expression = "util.find_spec(\""+module+"\") is not None";
        auto passed = (bool*) TPython::Eval(expression.c_str());
        return ((passed != NULL)&&(*passed));
    }
    
    template <typename T>
    inline T GetPyResult(const std::string &module, const std::string &method, const std::string &input,
        const std::string & path = ""){
        if (!PyModuleExists(module, path))
            return T{};
        std::string expression = "from "+module+" import "+method;
        TPython::Exec(expression.c_str());
        expression = method+"(\""+input+"\")";
        T* result = (T*) TPython::Eval(expression.c_str());
        gROOT->GetListOfClassGenerators()->Clear();
        if (result == NULL)
            return T{};
        return *result;             
    }        

    template <typename T>
    inline T GetPyResultHere(const std::string &module, const std::string &method, const std::string &input){
        return GetPyResult<T>(module,method,input, GetCurrentDirectory());     
    }           

}   // namespace CPy
}   // namespace Alkaid





#endif