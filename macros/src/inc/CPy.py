import numpy as np
import ROOT
import glob
import os
from pdb import set_trace

# the c++ based vector type is not recognized by python so it needs to be unwrapped as numpy array first
def UnwrapRVec(data):
    # you may include more supported types as you need
    RVecTypes = (type(( ROOT.ROOT.VecOps.RVec("float")())),
    type(( ROOT.ROOT.VecOps.RVec("int")())),type(( ROOT.ROOT.VecOps.RVec("uint")())))

    result = {}
    for arr in data:
        if len(data[arr]) > 0:
            if isinstance(data[arr][0],RVecTypes):
                result[arr] = np.array([np.asarray(x) for x in data[arr]])
            else:
                result[arr] = data[arr]
    return result

# because the c++ implementation of RDataFrame does not support AsNumpy() method, need to do it here
def RootToNumpy(filename, treename):
    print("INFO: Preparing conversion of "+filename+" to npz file ...")
    if RootFileIsEmpty(filename):
        print("INFO: The root file \""+filename+"\" is empty. Conversion skipped.")
        return True
    data = ROOT.ROOT.RDataFrame(treename,filename).AsNumpy()
    data = UnwrapRVec(data)
    save_filename = filename.replace(".root","")
    np.savez(save_filename,**data)
    print("INFO: Succesfully converted "+filename+" to npz file as "+save_filename+".npz")
    return True

def RootFileIsEmpty(filename):
    f = ROOT.TFile(filename)
    return f.GetListOfKeys().GetSize() == 0

def PyListToCVector(input):
    result = ROOT.vector("string")()
    result += [str(i) for i in input]
    return result

def CVectorToPyList(input):
    return [i for i in input]

def CMapToDict(input):
    data = {}
    for m in input:
        data[m.first] = np.asarray(m.second)
    return data

def EmptyCMap():
    cmap = ROOT.map("string,vector<string>")()
    carray = ROOT.vector("string")()
    cmap[""] = carray
    return cmap

def DictToCMap(input):
    cmap = ROOT.map("string,vector<string>")()
    for key in input:
        carray = ROOT.vector("string")()
        # Insert list elements as vector of strings
        if type(input[key]) == list:
            carray += [str(i) for i in input[key]]
        # Insert numpy array element as vector of strings
        elif type(input[key]) == np.ndarray:
            # check if the value is not an array
            if input[key].ndim == 0:
                carray += [str(input[key].item())]
            elif input[key].ndim == 1:
                # convert elements to string type
                carray += input[key].astype('U')
        # Be aware of unexpected behaviours when casting unexpcted types into string
        else:
            carray += [str(input[key])]
        cmap[key] = carray
    if (cmap.size() == 0):
        return EmptyCMap()
    return cmap   

def NpzToCMap(input):
    # simple check for the validity of input file
    if (not os.path.exists(input)) or (not MatchExt(input,"npz")):
        return ROOT.map("string,vector<string>")()
    data = dict(np.load(input, allow_pickle = True))
    return DictToCMap(data)  

def GetFileNamesDict(input, extension = ""):
    if (os.path.exists(input)):
        if (MatchExt(input,"npz")):
            return GetFileNamesFromNpz(input,extension)
        elif (MatchExt(input,"npy")):
            return CVectorToCMap(GetFileNamesFromNpy(input,extension))
        elif MatchExt(input,extension):
            return DictToCMap({"":[input]})
    return CVectorToCMap(GetFileNameGlobs(input,extension))

def CVectorToCMap(input,key = ""):
    cmap = ROOT.std.map("string,vector<string>")()
    cmap[key] = input
    return cmap

def GetFileNameGlobs(input, extension = ""):
    return PyListToCVector([i for i in glob.glob(input) if MatchExt(i,extension)])

def GetFileNamesFromNpy(input, extension = ""):
    result = ROOT.vector("string")()
    if (not os.path.exists(input)) or (not MatchExt(input,"npy")):
        return result
    data = np.load(input)    
    for datum in data:
        result += [i for i in glob.glob(datum) if MatchExt(i,extension)]
    return result


def NestedNpzToCMap(input):
    cnestedmap = ROOT.map("string,map<string,vector<string>>")()
    if (not os.path.exists(input)) or (not MatchExt(input,"npz")):
        return cnestedmap
    layer1 = np.load(input,allow_pickle=True)
    for key1 in layer1:
        if (layer1[key1].ndim is not 0):
            return cnestedmap
        layer2 = layer1[key1].item()
        if type(layer2) is not dict:
            return cnestedmap
        cnestedmap[key1] = DictToCMap(layer2)
    return cnestedmap 

def MatchExt(filename, extension):
    return (extension == "") or (os.path.splitext(filename)[1] == "."+extension)

def GetFileNamesFromNpz(input, extension = ""):
    if (not os.path.exists(input)) or (not MatchExt(input,"npz")):
        return ROOT.map("string,vector<string>")()  

    data = dict(np.load(input, allow_pickle = True))
    BasePath = ""
    if "BasePath" in data:
        if data["BasePath"].shape == ():
            BasePath = data["BasePath"].item()+"/"
        else:
            BasePath = data["BasePath"][0]+"/"
    data.pop("BasePath",None)
    for key in data:
        values = data[key]
        if type(values) is list:
            data[key] = [j for i in values for j in glob.glob(BasePath+str(i)) if MatchExt(j,extension)]
        elif type(values) is np.ndarray:
            if values.ndim == 0:
                data[key] = [i for i in glob.glob(BasePath + str(values)) if i.endswith(extension)]
            elif values.ndim == 1:
                data[key] = [j for i in values for j in glob.glob(BasePath+str(i)) if MatchExt(j,extension)]
        else: 
            data[key] = glob.glob(BasePath + str(values))

    return DictToCMap(data)