// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019
#ifndef _ARegEx_
#define _ARegEx_

// system include(s)
#include <regex>

namespace Alkaid{
	namespace RegEx{
		const std::regex kRealNumber = std::regex("((\\+|\\-)?[0-9]+)(.(([0-9]+)?))?((e|E)((\\+|\\-)?)[0-9]+)?");
		const std::regex kVariable = std::regex("[a-zA-Z_]\\w*");
		const std::regex kAlphaNumeric = std::regex("\\w+");
		const std::regex kSubscript = std::regex("([a-zA-Z_][\\w.]*)\\[(\\d+)\\]");
		const std::regex kInteger = std::regex("(\\+|\\-)?[0-9]+");
		const std::regex kCharacters = std::regex("[a-zA-Z]+");
		const std::regex kMember = std::regex("([a-zA-Z_]\\w*)\\.([a-zA-Z_]\\w*)");
		const std::regex kFunctionName = std::regex("[a-zA-Z_]\\w*(?=\\()");
		const std::regex kVariableOnly = std::regex("^[\\s]*[a-zA-Z_]\\w*[\\s]*$");
		const std::regex kVariableOnlyEx = std::regex("^[\\s]*[a-zA-Z_\\$][\\w\\$.]*[\\s]*$");
		const std::regex kDefineList = std::regex("([a-zA-Z_]\\w*)[\\s]*=[\\s]*\\[([a-zA-Z_0-9,./\\- \t]*)\\]");
		const std::regex kDefineListEx = std::regex("([a-zA-Z_\\$][\\w\\$.]*)[\\s]*=[\\s]*\\[([a-zA-Z_0-9,./\\- \t\\$]*)\\]");
		const std::regex kEquation = std::regex("([a-zA-Z_]\\w*)[\\s]*=(.*)");
		const std::regex kEquationEx = std::regex("([$a-zA-Z_][\\w\\$.]*)[\\s]*=(.*)");
		const std::regex kBraces = std::regex("\\{(.*)\\}");
		const std::regex kDoubleBraces = std::regex("\\{\\{(.*)\\}\\}");
		const std::regex kLeadingEquation = std::regex("^[\\s]*([a-zA-Z_]\\w*)[\\s]*=(.*)");
		const std::regex kLeadingEquationEx = std::regex("^[\\s]*([a-zA-Z_\\$][\\w\\$.]*)[\\s]*=(.*)");
		const std::regex kAlias = std::regex("([a-zA-Z_]\\w*)[\\s]*=[\\s]*([a-zA-Z_]\\w*)");
		const std::regex kAliasEx = std::regex("([a-zA-Z_\\$][\\w\\$.]*)[\\s]*=[\\s]*([a-zA-Z_\\$][\\w\\$.]*)");
		const std::regex kAliasOnly = std::regex("^[\\s]*([a-zA-Z_]\\w*)[\\s]*=[\\s]*([a-zA-Z_]\\w*)[\\s]*$");
		const std::regex kAliasOnlyEx = std::regex("^[\\s]*([a-zA-Z_\\$][\\w\\$.]*)[\\s]*=[\\s]*([a-zA-Z_\\$][\\w\\$.]*)[\\s]*$");		
		const std::regex kMoney = std::regex("\\$([\\w_.]+)\\$");
	}
}

#endif