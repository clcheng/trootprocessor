// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019
#ifndef _AUtility_
#define _AUtility_

// system include(s)
#include<map>
#include<map>
#include<set>
#include<regex>
#include<string>
#include<vector>
#include<fstream>
#include<sstream>
#include<iostream>
#include<iterator>
#include<algorithm>

// ROOT include(s)
#include "TPython.h"
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TStopwatch.h"

// local include(s)
#include "ACPy.h"
#include "AType.h"
#include "ARegEx.h"

namespace Alkaid{
namespace Utility{
    const int kTrimWhitespace = 0b1;
    const int kReduceWhitespace = 0b10;
    const int kRemoveCarrierReturn = 0b100;
    const int kClean = 0b1000;

    template <typename Map1, typename Map2>
    inline bool MapsHaveSameKeys (Map1 const &lhs, Map2 const &rhs) {
        auto pred = [] (auto a, auto b)
                       { return a.first == b.first; };
        return (lhs.size() == rhs.size())
            && std::equal(lhs.begin(), lhs.end(), rhs.begin(), pred);
    }        

    template<typename T>
    inline std::vector<T> Flatten2DVector(const std::vector<std::vector<T>> &v2D){   
        std::vector<T> result;
        for(const auto &v: v2D)
            result.insert(result.end(), v.begin(), v.end());                                                                                         
        return result;
    }  

    template<typename T>
    inline std::vector<T> &VectorSortRemoveDuplicates(std::vector<T> & vec){
        std::set<T> s( vec.begin(), vec.end() );
        vec.assign( s.begin(), s.end() );
        return vec;
    }

    template<typename T>
    inline auto VectorFindElement(const std::vector<T> &vec,const T &value){
        return std::find(vec.begin(),vec.end(),value);
    }

    template<typename T>
    inline bool VectorContains(const std::vector<T> &vec,const T &value){
        return VectorFindElement(vec,value)!=vec.end();
    }

    template<typename T>
    inline std::vector<T> & VectorBackInsert(std::vector<T> &target,const std::vector<T> &source){
        target.insert(target.end(),source.begin(),source.end());
        return target;
    }


    template<typename T>
    inline void VectorAddIfNotExist(std::vector<T> &vec, const T &value){
        if (!VectorContains(vec,value))
            vec.emplace_back(value);
    }

    template<typename T1, typename T2>
    inline void AddElementToVectorMap(std::map<T1,std::vector<T2>> &m, const T1 &key, const T2 &value){
        if (m.count(key)==0) // or m.find(key) == m.end()
            m.emplace(key,std::vector<T2>({value}));
        else
            m[key].push_back(value);
    }

    template<typename T>
    inline std::vector<T> VectorIntersection(std::vector<T> v1, std::vector<T> v2){
        std::vector<T> v3;
        std::sort(v1.begin(), v1.end());
        std::sort(v2.begin(), v2.end());
        std::set_intersection(v1.begin(),v1.end(),
                              v2.begin(),v2.end(),
                              back_inserter(v3));
        return v3;
    }

    template<typename T>
    inline std::vector<T> VectorDifference(std::vector<T> v1, std::vector<T> v2){
        std::vector<T> v3;
        std::sort(v1.begin(), v1.end());
        std::sort(v2.begin(), v2.end());
        std::set_difference(v1.begin(), v1.end(), v2.begin(), v2.end(),
                back_inserter(v3));
        return v3;
    }        

    

    template <typename T>
    inline bool VectorIsSubset(std::vector<T> v1,std::vector<T> v2){
        std::sort(v1.begin(), v1.end());
        std::sort(v2.begin(), v2.end());
        return std::includes(v1.begin(), v1.end(), v2.begin(), v2.end());
    }        


    inline bool PrintMessage(const std::string_view s, const bool rtn = false){
            std::cout<<s<<"\n";
        return rtn;
    }

    inline bool PrintErrorMessage(const std::string_view s, const bool rtn = false){
        if (!rtn)
            std::cerr<<s<<"\n";
        return rtn;
    }        

    inline void PrintStopwatchTime(TStopwatch &stopwatch){
        Double_t realtime = stopwatch.RealTime();
        Double_t cputime = stopwatch.CpuTime();
        printf("Processing Time : Real Time = %7.3f s, CPU Time = %7.3f s\n\n\n",realtime,cputime);
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////  C++ and Python Interface
/////////////////////////////////////////////////////////////////////////////////////////////////////
    inline Alkaid::Type::Dict NpzToCMap(const std::string &input){
        return CPy::GetPyResultHere<Type::Dict>("CPy","NpzToCMap",input);
    }
    inline Alkaid::Type::NestedDict NestedNpzToCMap(const std::string &input){
        return CPy::GetPyResultHere<Type::NestedDict>("CPy","NestedNpzToCMap",input);
    }
    inline Alkaid::Type::Dict GetFileNamesDict(const std::string &input,
        const std::string &extension = ""){
        return CPy::GetPyResultHere<Type::Dict>("CPy","GetFileNamesDict",input+"\",\""+extension);
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

    inline bool StrIsBlank(const std::string& s){
        return s.find_first_not_of(" \t\n\v\f\r")==std::string::npos;
    }

    std::vector<std::string> StrSplit(const std::string &str, const std::string &delim =" ");

    inline std::string &TrimAfterSubStr(std::string &s, const std::string &sub){
        const size_t pos = s.find(sub);
        return ((sub=="")||(pos == std::string::npos)) ? s : (s = s.substr(0,pos));
    }

    inline std::string &TrimAfterSubStr(std::string &s, const std::vector<std::string> &subVec){
        size_t pos;
        for (auto &sub: subVec)
            if ((pos = s.find(sub))!= std::string::npos)
                return (s = s.substr(0,pos));
        return s;
    }        

    inline std::string &TrimBeforeSubStr(std::string &s, const std::string &sub){
        const size_t pos = s.find(sub);
        return ((sub=="")||(pos == std::string::npos)) ? s : (s = s.substr(pos+1));
    }     

    inline std::string &TrimBeforeSubStr(std::string &s, const std::vector<std::string> &subVec){
        size_t pos;
        for (auto &sub: subVec)
            if ((pos = s.find(sub))!= std::string::npos)
                return (s = s.substr(pos+1));
        return s;
    }          

    inline std::string StrRemoveAll(const std::string &s,const char &c){
        std::string ss = s;
        ss.erase( std::remove(ss.begin(), ss.end(), c), ss.end() );
        return ss;
    }

    inline std::string StrReplaceAll(const std::string &target,const std::string &s1,
        const std::string &s2){
        return (std::string)((TString)target).ReplaceAll(s1.c_str(),s2.c_str());
    }        

    std::string StrTrim(const std::string& str, const std::string& whitespace = " \t");

    std::string StrReduce(const std::string& str, const std::string& fill = " ", 
        const std::string& whitespace = " \t");            

    std::string StrClean(const std::string &s, const int &option = kClean);

    void VStrClean(std::vector<std::string>&vstr, const int &option = kClean);
    void VStrClean(std::vector<std::vector<std::string>>&vstr, const int &option = kClean);

    std::vector<std::vector<std::string>> GetMatchedStr(const std::string &s, const std::regex &re);
    std::vector<std::string> GetMatchedSubStr(const std::string &s, const std::regex &re, const size_t pos);
    bool IsMatchRegEx(const std::string &s, const std::regex &re);

    std::vector<std::string> GetLinesFromFile(const std::string &inFile, 
        const std::string &commentStr = "#", const int &option = kClean, const bool rmEmpty = true);

    template<typename T>
    inline std::string VectorToStr(const std::vector<T> &v, const std::string &delim = ", "){
        std::ostringstream vts; 
          if (!v.empty()) { 
            std::copy(v.begin(), v.end()-1, std::ostream_iterator<T>(vts, delim.c_str())); 
            vts << v.back(); 
          }
        return vts.str();
    }

    std::vector<Alkaid::Type::StrPair> GetStrPairsFromFile(
        const std::string &inFile,
        const std::vector<std::string> keys = {},
        const std::string &separator = ":",
        const Alkaid::Type::StrPair &braces = Alkaid::Type::StrPair("{{","}}"),
        const std::string &commentStr = "#",
        const int &option = kClean);

    Alkaid::Type::Dict GetMappedStrFromFile(
        const std::string &inFile,
        const std::vector<std::string> keys = {},
        const std::string &separator = ":",
        const Alkaid::Type::StrPair &braces = Alkaid::Type::StrPair("{{","}}"),
        const std::string &commentStr = "#",
        const int &option = kClean);

    Alkaid::Type::Dict GetMappedStrFromFile(
        const std::vector<Alkaid::Type::StrPair> &str_pairs);  

    std::vector<std::string> GetVarNamesInStr(const std::string &s);
    std::vector<std::string> GetVarNamesInStr(const std::vector<std::string> &sVec);
    
} // namespace Utility

} // namespace Alkaid


#endif