// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019
#ifndef _TDataFrameWrapper_
#define _TDataFrameWrapper_


// ROOT include(s)
#include <ROOT/RDataFrame.hxx>

class TDataFrameWrapper{
	public: 
		typedef ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter, void> RIJittedFilter;
		typedef ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager, void> RILoopManager;
		typedef ROOT::RDF::RResultPtr<ROOT::RDF::RCutFlowReport> RICutFlowReport;
		typedef ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager>> SnapRet_t;

		const int kDefault = -1;
		const int kBase = 0;
		const int kLInterface = 1;	
		const int kFInterface = 2;

		void Clear();
		bool IsEmpty(){return (InterfaceID == kDefault);}		
		RILoopManager GetLInterface();
		RIJittedFilter GetFInterface();
		ROOT::RDataFrame GetBase();
		int GetInterfaceID()const;
		TDataFrameWrapper& Define(std::string_view name, std::string_view expression);
		TDataFrameWrapper& Filter(std::string_view expression, std::string_view name = "");
		TDataFrameWrapper& Alias(std::string_view alias, std::string_view columnName);
		SnapRet_t Snapshot(std::string_view treename, std::string_view filename,
			const std::vector<std::string> & columnList , const bool & lazy = true );
		SnapRet_t Snapshot(std::string_view treename, std::string_view filename,
			std::string_view columnList = "", const bool & lazy = true );

		std::vector<std::string> GetColumnNames();		

		RICutFlowReport Report();

		// Default Constructor
		TDataFrameWrapper():
			fInterface(ROOT::RDataFrame(0).Filter("true")),
			lInterface(ROOT::RDataFrame(0).Cache({})),
			base(ROOT::RDataFrame(0)),InterfaceID(kDefault){}

		// Copy constructor
		TDataFrameWrapper(const TDataFrameWrapper &copy):
			lInterface(copy.lInterface), fInterface(copy.fInterface),
			base(copy.base), InterfaceID(copy.InterfaceID){}

		TDataFrameWrapper(const ROOT::RDataFrame &copy):
			lInterface(ROOT::RDataFrame(0).Cache({})), fInterface(ROOT::RDataFrame(0).Filter("true")),
			base(copy), InterfaceID(kBase){}			

		TDataFrameWrapper(const RILoopManager &copy):
			lInterface(copy), fInterface(ROOT::RDataFrame(0).Filter("true")),
			base(ROOT::RDataFrame(0)), InterfaceID(kLInterface){}		

		TDataFrameWrapper(const RIJittedFilter &copy):
			lInterface(ROOT::RDataFrame(0).Cache({})), fInterface(copy),
			base(ROOT::RDataFrame(0)), InterfaceID(kFInterface){}

		// Operator Overloading
		TDataFrameWrapper& operator= (const TDataFrameWrapper &other);
	private:
		RILoopManager lInterface;	//LoopManager-Like Data Frame Interface
		RIJittedFilter fInterface;	//Filter-Like Data Frame Interface
		ROOT::RDataFrame base;	//Base data frame
		int InterfaceID;	//The Current Interface in Use		
};


#endif