// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019
#ifndef _AType_
#define _AType_

// system include(s)
#include<map>
#include<string>
#include<vector>

namespace Alkaid{
namespace Type{

    typedef std::pair<std::string,std::string> StrPair; 
    typedef std::map<std::string,std::vector<std::string>> Dict; 
    typedef std::map<std::string,std::map<std::string,std::vector<std::string>>> NestedDict;          

} // namespace Type
} // namespace Alkaid


#endif