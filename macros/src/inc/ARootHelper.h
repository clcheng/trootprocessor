#ifndef _ARootHelper_
#define _ARootHelper_

#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "FileSystem.h"
#include <ROOT/RDataFrame.hxx>


namespace Alkaid{
	namespace RootHelper{
		bool CheckRootFileFormat(const std::string& file_name, const std::string & tree_name, const std::vector<std::string> column_names = {});
		bool CheckRootFileFormat(const std::vector<std::string> &file_names, const std::string & tree_name, const std::vector<std::string> column_names = {});
		bool RootFileIsValid(const std::string& file_name);
		bool RootFileHasTree(const std::string& file_name, const std::string &tree_name);
	}
}


#endif