// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019
#ifndef _ACustomFunction_
#define _ACustomFunction_

// system include(s)
#include<map>
#include<string>
#include<vector>

// ROOT include(s)
#include "TInterpreter.h"


namespace Alkaid{
namespace CustomFunction{

    const std::map<std::string, std::string> kCustomFunctionMap = 
    {
        {"Namespace_ROOT_VecOps",
            "using namespace ROOT::VecOps;"},
        {"Namespace_ROOT_Math",
            "using namespace ROOT::Math;"},
        {"SizeOf",
            "template<typename T>"
            "auto SizeOf(T vec)"
            "{ return vec.size(); }"},
        {"Sort_and_Take",
            "template<typename T1, typename T2, typename T3>"
            "auto Sort_and_Take(T1 vec, T2 idx, T3 len)"
            "{ return Take(Take(vec,idx),len); }"},
        {"Ascending_Indices",
            "template<typename T>"
            "auto Ascending_Indices(T vec)"
            "{ return Argsort(vec); }"},
        {"Descending_Indices",
            "template<typename T>"
            "auto Descending_Indices(T vec)"
            "{ return Reverse(Argsort(vec)); }"},          
        {"MatchAnyInt",
            "template<typename T>"
            "auto MatchAnyInt(ROOT::VecOps::RVec<Int_t> vec, T value)"
            "{ return Any(vec == value); }"},       
        {"DeltaPHI",
            "template<typename T1, typename T2>"
            "auto DeltaPHI(T1 phi1, T2 phi2)"
            "{ return ROOT::VecOps::DeltaPhi(phi1,phi2);}"},
        {"CosineThetaStar",
            "template<typename T1, typename T2>"
            "auto CosineThetaStar(T1 P1, T2 P2){"
            "auto P12 = P1 + P2;"
            "double P1_plus  = (P1.E() + P1.Pz())/1.41421356;"
            "double P2_plus  = (P2.E() + P2.Pz())/1.41421356;"
            "double P1_minus = (P1.E() - P1.Pz())/1.41421356;"
            "double P2_minus = (P2.E() - P2.Pz())/1.41421356;"
            "double M12_squared = pow(P12.M(),2);"
            "double PT12_squared = pow(P12.Pt(),2);"
            "return 2*((P1_plus*P2_minus -P1_minus*P2_plus)/"
            "sqrt(M12_squared*(M12_squared+PT12_squared)))*(P12.Pz()/abs(P12.Pz()));}"},
        {"L4V_PtEtaPhiM",
            "template<typename T1, typename T2, typename T3, typename T4>"
            "auto L4V_PtEtaPhiM(T1 pt, T2 eta, T3 phi, T4 m)"
            "{ return ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<Float_t>>(pt,eta,phi,m);}"},  
        {"L4V_PtEtaPhiE",
            "template<typename T1, typename T2, typename T3, typename T4>"
            "auto L4V_PtEtaPhiE(T1 pt, T2 eta, T3 phi, T4 e)"
            "{ return ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<Float_t>>(pt,eta,phi,e);}"},               
        {"L4V_PxPyPzM",
            "template<typename T1, typename T2, typename T3, typename T4>"
            "auto L4V_PxPyPzM(T1 px, T2 py, T3 pz, T4 m)"
            "{ return ROOT::Math::LorentzVector<ROOT::Math::PxPyPzM4D<Float_t>>(px,py,pz,m);}"},
        {"DescendingSortByColumn",
            "template<typename T>"
            "void DescendingSortByColumn(ROOT::VecOps::RVec<ROOT::VecOps::RVec<T>> &source, const Int_t &column)"
            "{ std::sort(source.begin(), source.end(), [&, column](const ROOT::VecOps::RVec<T> &a,"
            " const ROOT::VecOps::RVec<T> &b){ return a[column] > b[column]; }); }"},
        {"AscendingSortByColumn",
            "template<typename T>"
            "void AscendingSortByColumn(ROOT::VecOps::RVec<ROOT::VecOps::RVec<T>> &source, const Int_t &column)"
            "{ std::sort(source.begin(), source.end(), [&, column](const ROOT::VecOps::RVec<T> &a,"
            " const ROOT::VecOps::RVec<T> &b){ return a[column] < b[column]; }); }"},  
        {"GetEFromPtEtaPhiM_1",
            "template<typename T1, typename T2, typename T3, typename T4>"
            "auto GetEFromPtEtaPhiM(const ROOT::VecOps::RVec<T1> &Pt, const ROOT::VecOps::RVec<T2> &Eta,"
            "const ROOT::VecOps::RVec<T3> &Phi, const ROOT::VecOps::RVec<T4> &M)"
            "{ auto result = ROOT::VecOps::RVec<Float_t>{};"
            " UInt_t NCol = Pt.size();"
            " for (UInt_t i = 0 ; i < NCol; i++)"
            " result.emplace_back(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<Float_t>>"
            "(Pt.at(i),Eta.at(i),Phi.at(i),M.at(i)).E());"
            " return result; }"},  
        {"GetEFromPtEtaPhiM_2",
            "template<typename T1, typename T2, typename T3>"
            "auto GetEFromPtEtaPhiM(const ROOT::VecOps::RVec<T1> &Pt, const ROOT::VecOps::RVec<T2> &Eta,"
            "const ROOT::VecOps::RVec<T3> &Phi, Float_t &M)"
            "{ auto result = ROOT::VecOps::RVec<Float_t>{};"
            " UInt_t NCol = Pt.size();"
            " for (UInt_t i = 0 ; i < NCol; i++)"
            " result.emplace_back(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<Float_t>>"
            "(Pt.at(i),Eta.at(i),Phi.at(i),M).E());"
            " return result; }"},
        {"GetMFromPtEtaPhiE",
            "template<typename T1, typename T2, typename T3, typename T4>"
            "auto GetMFromPtEtaPhiE(const ROOT::VecOps::RVec<T1> &Pt, const ROOT::VecOps::RVec<T2> &Eta,"
            "const ROOT::VecOps::RVec<T3> &Phi, const ROOT::VecOps::RVec<T4> &E)"
            "{ auto result = ROOT::VecOps::RVec<Float_t>{};"
            " UInt_t NCol = Pt.size();"
            " for (UInt_t i = 0 ; i < NCol; i++)"
            " result.emplace_back(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<Float_t>>"
            "(Pt.at(i),Eta.at(i),Phi.at(i),E.at(i)).M());"
            " return result; }"},
        {"Transpose_1",
            "template<typename T>"
            "auto Transpose(const ROOT::VecOps::RVec<ROOT::VecOps::RVec<T>> &data) {"
            " if ((data.size() == 0)||(data[0].size()==0))"
            " return ROOT::VecOps::RVec<ROOT::VecOps::RVec<T>>{};"
            " ROOT::VecOps::RVec<ROOT::VecOps::RVec<T>> result(data[0].size(),std::vector<T>(data.size()));"
            " for (UInt_t i = 0; i < data[0].size(); i++)"
            " for (UInt_t j = 0; j < data.size(); j++)"
            " result[i][j] = data[j][i];"
            " return result; }"},
        {"Transpose_2",
            "template<typename T>"
            "auto Transpose(const std::initializer_list<T> &data) {"
            " return Transpose(ROOT::VecOps::RVec<T>{data}); }"}            
    };

    std::string CreateDeclaration(const std::string & name, const std::string & declaration){
        const std::string nameguard = "_customfunction_"+name+"_";
        const std::string expression = "#ifndef "+nameguard+" \n #define "+nameguard+
                " \n "+declaration+" \n #endif \n";
        return expression;
    }

    void DeclareAllCustomFunctions(){
        for (auto &customfunction: kCustomFunctionMap)
            gInterpreter->Declare(CreateDeclaration(customfunction.first,
                customfunction.second).c_str());
    }

    void DeclareCustomFunction(const std::string &function_name){
        if (kCustomFunctionMap.count(function_name) != 0)
            gInterpreter->Declare(CreateDeclaration(function_name,
                kCustomFunctionMap.at(function_name)).c_str());            
        else
            std::cout<<"WARNING: Unknown custom function \""<<function_name<<"\". Skipped.\n";
    }    

}   // namespace CustomFunction
}   // namespace Alkaid

#endif