// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019
#ifndef _TFileManager_
#define _TFileManager_

// system include(s)
#include <regex>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <sys/stat.h>

// ROOT include(s)
#include "TList.h"
#include "TPython.h"
#include "TString.h"
#include "TSystem.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TSystemFile.h"
#include "TSystemDirectory.h"

// local include(s)
#include "ACPy.h"
#include "AType.h"
#include "AUtility.h"

namespace Alkaid{
    
namespace FileManager{
    
    inline bool IsDirectory(const std::string& p){
        struct stat s;
        return (stat (p.c_str(), & s) == 0) && (s.st_mode & S_IFDIR);
    }

    inline bool IsDirectory(const TSystemFile *File) {
        return (File->IsDirectory() && (TString(File->GetName()) != ".") && (TString(File->GetName()) != ".."));
    }

    inline bool IsRegularFile(const std::string& p){
        struct stat s;
        return (stat (p.c_str(), & s) == 0) && (s.st_mode & S_IFREG);
    }

    inline std::string GetFileExtension(const std::string &f) {
        const size_t pos = f.find_last_of(".");
        const size_t pos2 = f.find_last_of("/");
        return (pos == std::string::npos) || (( pos2 != std::string::npos) && (pos2 > pos))?  "": f.substr(pos + 1) ;
    }


    inline std::string GetFullPath(const TSystemFile &f){
        std::string fdir = (std::string) f.GetTitle();
        std::string fname = (std::string) f.GetName();
        return (std::string)(fdir+'/'+fname);
    }

    inline std::string GetFileDirectory(const std::string &f) {
        if ((f.rfind("./", 0) == 0)||(f.rfind("~/", 0) == 0))
            return "";
        const size_t pos = f.find_last_of("\\/");
        return (std::string::npos == pos) ? "" : f.substr(0, pos);
    }

    inline std::string GetFileName(const std::string &f) {
        return gSystem->BaseName(f.c_str());
    }

    inline void WriteTextToLogFile( const std::string &logfile, const std::string &text ){
        std::ofstream log_file(
            logfile.c_str(), std::ios_base::out | std::ios_base::app );
        log_file << text << "\n";
    }


    struct TSystemFileComparer
    {
        bool operator()( const TSystemFile& l, const TSystemFile& r ) const {
            return GetFullPath(l) < GetFullPath(r);
        }
    };

    inline bool TSystemFileIsUnique (const TSystemFile& a, const TSystemFile& b) {
        return (GetFullPath(a) == GetFullPath(b));
    }

    inline void MakeDirs(const std::string &dir){
        std::string expr = "mkdir -p "+dir;
        gSystem->Exec(expr.c_str());
    }

    inline void MakeDirsForFile(const std::string &filepath){  
        std::string expr = "mkdir -p $(dirname "+filepath+")";
        gSystem->Exec(expr.c_str());
    }

///////////////////////////////////////////////////////////////////////////////////////////
// Python Interface
///////////////////////////////////////////////////////////////////////////////////////////
    inline std::vector<std::string> GetFileNameGlobs(const std::string &input,
        const std::string &extension =""){
        return CPy::GetPyResultHere<std::vector<std::string>>("CPy","GetFileNameGlobs",input+"\",\""+extension); 
    }

    inline Alkaid::Type::Dict GetFileNamesFromNpz(const std::string &input,
        const std::string &extension = ""){
        return CPy::GetPyResultHere<Type::Dict>("CPy","GetFileNamesFromNpz",input+"\",\""+extension);
    }

    inline Alkaid::Type::Dict GetFileNamesDict(const std::string &input,
        const std::string &extension = ""){
        return CPy::GetPyResultHere<Type::Dict>("CPy","GetFileNamesDict",input+"\",\""+extension);
    }

///////////////////////////////////////////////////////////////////////////////////////////
// Class Object
///////////////////////////////////////////////////////////////////////////////////////////
    class TFileCollection{
        private:
            const std::regex kRe = std::regex("RegEx:[ \t]+(.*)");
            int verbose;
            std::string expr;
            std::string currentPath;
            void Clean();
        protected:
            std::vector<TSystemFile> files;
        public:
            //Construct iteractor to allow auto for loop iteration
            typedef std::vector<TSystemFile>::iterator iterator;
            typedef std::vector<TSystemFile>::const_iterator const_iterator;
            iterator begin() { return this->files.begin(); }
            iterator end() { return this->files.end(); }
            //Clear files and free memory
            inline void Clear(){
                std::vector<TSystemFile>().swap(this->files);   
            }
            inline TFileCollection& operator+=(const TFileCollection& a){
                this->files.insert(this->files.end(),a.files.begin(),a.files.end());
                this->Clean();
                return *this;
            }
            inline TSystemFile& operator[](int idx){
                return this->files[idx];
            }        
            inline bool Add(const std::string &fname, bool cleaning = true){
                return this->Add(GetFileName(fname), GetFileDirectory(fname), cleaning);
            }
            inline bool Add(const TFileCollection& a) {
                *this += a;
                return true;
            }
            inline ULong_t Size() {
                return files.size();
            }
            inline TFileCollection& operator=(const TFileCollection& a) {
                if (this != & a)
                    this->files = std::vector<TSystemFile>(a.files);
                return *this;
            }
            friend TFileCollection operator+(TFileCollection a, const TFileCollection& b) {
                a += b;
                return a;
            }
            inline void Show(){
                for (auto file:this->files)
                    std::cout<<GetFullPath(file)<<"\n";
            }
            inline void SetRegEx(std::string expr){
                this->expr = expr;
            }                
            bool Add(const std::string &fname,const std::string &fdir, bool cleaning = true);
            bool LoadFromObject(const std::string input, const std::string ext = "", const bool wRecursive = true);
            bool LoadFromDataCard(const std::string input, const std::string ext = "", const bool wRecursive = true);                
            bool LoadFromDirectory(const std::string input, const std::string ext = "", const bool wRecursive = true,
                bool cleaning = true);             
            std::vector<std::string> GetFileNameGlobs();
            TFileCollection(const int verbose = 1): verbose(verbose),expr(""){}          
            TFileCollection(const std::string input, const std::string ext = "", const bool wRecursive = true, 
                const int verbose = 1): verbose(verbose),expr("") {
                LoadFromObject(input, ext, wRecursive);
            }
    };

    class TRootFileCollection: public TFileCollection{
            public:
                inline bool LoadFromObject(const std::string input,  const bool wRecursive = true){
                    return TFileCollection::LoadFromObject(input,"root",wRecursive);
                }
                inline bool LoadFromDataCard(const std::string input, const bool wRecursive = true){
                    return TFileCollection::LoadFromDataCard(input,"root",wRecursive);
                }                    
                inline bool LoadFromDirectory(const std::string input, const bool wRecursive = true,
                bool cleaning = true){
                    return TFileCollection::LoadFromDirectory(input,"root",wRecursive, cleaning);
                }
                TRootFileCollection(const int verbose = 1):TFileCollection(verbose){}
                TRootFileCollection(const std::string input, const bool wRecursive = true, const int verbose = 1): 
                    TFileCollection(input, "root", wRecursive, verbose) {
                }
    };



} // namespace FileManager

} // namespace Alkaid

#endif
