// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019
#ifndef _TRootProcessor_
#define _TRootProcessor_

// system include(s)
#include <map>
#include <regex>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

// ROOT include(s)
#include "TPython.h"
#include "TSystem.h"
#include "TStopwatch.h"
#include "TInterpreter.h"
#include <ROOT/RDataFrame.hxx>

// local include(s)
#include "AType.h"
#include "ARegEx.h"
#include "AUtility.h"
#include "AFileManager.h"
#include "ACustomFunction.h"
#include "TDataFrameWrapper.h"


using namespace ROOT::VecOps;

namespace Alkaid{

class TRootProcessor{

  public: 

/////////////////////////////////////////////////////////////////
/////    Setters
/////////////////////////////////////////////////////////////////

    inline void SetTreeName(const std::string & tree_name){
        this->tree_name = tree_name;
    }


    inline void SetLogFileName(const std::string & logfile){
        this->log_filename = logfile;
    }    

/////////////////////////////////////////////////////////////////
/////    Getters
/////////////////////////////////////////////////////////////////
    inline std::string GetTreeName()const {
        return this->tree_name;
    }

    inline std::string GetLogFileName()const {
        return this->log_filename;
    }    

    inline Alkaid::Type::Dict GetInputFiles() const {
        return this->input_files;
    }   

    inline std::vector<Alkaid::Type::StrPair> GetRawActions() const{
        return this->actions_raw;
    }      

    inline std::vector<std::vector<std::string>> GetValidatedActions() const{
        return this->actions_validated;
    }

    inline std::vector<std::vector<std::string>> GetExtendedActions() const{
        return this->actions_extended;
    }

    inline Alkaid::Type::NestedDict GetExtensionData() const{
        return this->extension_data;
    }

/////////////////////////////////////////////////////////////////
/////    Public Methods
/////////////////////////////////////////////////////////////////    

    bool ResetAll();
    bool ResetSingle();
    bool StartOver(const std::string &input, const std::string &datacard,
        const std::string &logfile,
        const std::string &extension = "");
    bool LoadInputFiles(const std::string &input);
    bool LoadDataCard(const std::string & datacard);
    bool LoadExtension(const std::string &extension);
    bool LoadDelphesLib(const std::string & delphespath);
    bool LoadFinishedKeys(const std::string logfile);    
    bool LoadCustomFunction();        
    bool RunAll();
    bool RunSingle(const std::string &key = "");

    //Default Contructor
    TRootProcessor(){}

    TRootProcessor(const std::string &input, const std::string &datacard,
        const std::string &logfile = "Finished_Keys.txt",
        const std::string &extension = "",
        const std::string &delphespath = "");

  private:
    typedef bool (TRootProcessor::*format_action)(const Alkaid::Type::StrPair&,
        std::vector<std::vector<std::string>>&);

    using SnapRet_t = ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager>>;    

    const std::string kTreeName = "TREE_NAME";
    const std::string kDefine = "DEFINE";
    const std::string kAlias = "ALIAS";
    const std::string kFilter = "FILTER";
    const std::string kSaveAsRoot = "SAVE_AS_ROOT";
    const std::string kHist1D = "HIST1D";
    const std::string kHist2D = "HIST2D";
    const std::string kSaveFrame = "SAVE_FRAME";
    const std::string kLoadFrame = "LOAD_FRAME";
    const std::string kDefaultFrame = "CURRENT";

    const std::vector<std::string> kActionList = 
    {
        kTreeName,
        kDefine,
        kAlias,
        kFilter,
        kSaveAsRoot,
        kHist1D,
        kHist2D,
        kSaveFrame,
        kLoadFrame
    };    

    struct SaveInfo{
        std::string treename;
        std::string filename;
        SaveInfo(const std::string treename, const std::string &filename):
        treename(treename),filename(filename){}
    };


    std::string log_filename;
    std::string tree_name;
    Alkaid::Type::Dict input_files; 
    Alkaid::Type::NestedDict extension_data; 
    std::vector<std::string> finished_keys;
    std::vector<Alkaid::Type::StrPair> actions_raw;
    std::vector<std::vector<std::string>> actions_validated;
    std::vector<std::vector<std::string>> actions_extended;
    
    std::vector<Alkaid::Type::StrPair> &ReplaceSubscriptOperator(std::vector<Alkaid::Type::StrPair> &target);
    std::vector<Alkaid::Type::StrPair> &RemoveNewLine(std::vector<Alkaid::Type::StrPair> &target);


    bool ValidateDataCard(std::vector<std::vector<std::string>> &target);
    bool Validate_Define(std::vector<std::string> &s);
    bool Validate_Filter(std::vector<std::string> &s);
    bool ExtendDataCard(const std::string &key);

    bool FormatActions(const std::vector<Alkaid::Type::StrPair> &source,
        std::vector<std::vector<std::string>> &target);
    bool Format_SaveAsRoot(const Alkaid::Type::StrPair& data,
        std::vector<std::vector<std::string>>& target);
    bool Format_Define(const Alkaid::Type::StrPair& data,
        std::vector<std::vector<std::string>>& target);
    bool Format_Alias(const Alkaid::Type::StrPair& data,
        std::vector<std::vector<std::string>>& target);
    bool Format_SaveOrLoadFrame(const Alkaid::Type::StrPair& data,
        std::vector<std::vector<std::string>>& target);    
    bool Format_General(const Alkaid::Type::StrPair& data,
        std::vector<std::vector<std::string>>& target);     
    bool Action_Define(const std::vector<std::string> &actioninfo,
        std::map<std::string,TDataFrameWrapper>& frames);
    bool Action_Filter(const std::vector<std::string> &actioninfo,
        std::map<std::string,TDataFrameWrapper>& frames);
    bool Action_Alias(const std::vector<std::string> &actioninfo,
        std::map<std::string,TDataFrameWrapper>& frames);  
    bool Action_SaveAsRoot(const std::vector<std::string> &actioninfo, const std::string &key,
        std::map<std::string,TDataFrameWrapper>& frames, std::vector<SnapRet_t> &snapshots,
        std::vector<SaveInfo> &numpy_saveinfo);    
    bool Action_SaveFrame(const std::vector<std::string> &actioninfo,
        std::map<std::string,TDataFrameWrapper>& frames);
    bool Action_LoadFrame(const std::vector<std::string> &actioninfo,
        std::map<std::string,TDataFrameWrapper>& frames);
    bool Action_SaveAsNumpy(const std::string filename, const std::string treename);    

    std::map<std::string, format_action> format_action_map = {
        {kTreeName, &TRootProcessor::Format_General},
        {kDefine, &TRootProcessor::Format_Define},
        {kAlias, &TRootProcessor::Format_Alias},
        {kFilter, &TRootProcessor::Format_General},
        {kSaveAsRoot, &TRootProcessor::Format_SaveAsRoot},
        {kSaveFrame, &TRootProcessor::Format_SaveOrLoadFrame},
        {kLoadFrame, &TRootProcessor::Format_SaveOrLoadFrame},        
        {kHist1D, &TRootProcessor::Format_General},
        {kHist2D, &TRootProcessor::Format_General}
    };
};

}
#endif