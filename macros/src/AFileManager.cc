// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019

#include "inc/AFileManager.h"


namespace Alkaid{
namespace FileManager{

bool TFileCollection::Add(const std::string &fname,const std::string& fdir, bool cleaning){
	const std::string f = fdir+"/"+fname;
	if (IsRegularFile(f)){
		//Regular exprression Selection
		try {
			if ((expr!="")&&(!std::regex_match(f,std::regex((std::string)this->currentPath+"[/]+"+this->expr))))
    			return false; 
		} catch (std::regex_error& e) {
			 std::cerr << "Invalid Regular Expression: " <<(std::string)this->currentPath+"[/]+"+this->expr<<"\n";
		}
		
		this->files.emplace_back(TSystemFile(fname.c_str(),fdir.c_str()));        
		if (this->verbose == 1)
			std::cout<<"INFO: Loaded File: "<<f<<"\n";
		// Sort Files and Remove Duplicates
		if (cleaning)
			this->Clean();
	}
	else
		return false;
	return true;
}

// Sort Files and Remove Duplicates
void TFileCollection::Clean(){
	std::sort( this->files.begin(), this->files.end(), TSystemFileComparer() );
	std::vector<TSystemFile>::iterator clean_begin = std::unique( this->files.begin(), this->files.end(),TSystemFileIsUnique);
	for (std::vector<TSystemFile>::iterator it = clean_begin; it != this->files.end(); it++)
		std::cout<<"INFO: Duplicated File "<< GetFullPath(*it)<<" Removed.\n";
	this->files.erase( clean_begin, this->files.end() );
}

bool TFileCollection::LoadFromObject(const std::string input, const std::string ext, const bool wRecursive){
	this->currentPath = "";
    if (IsDirectory(input)){
    	//Update the input path to the current path so that the regular expression is applied to the new path
    	this->currentPath = input;
    	this->LoadFromDirectory(input, ext, wRecursive, false);
    }
    else if (IsRegularFile(input)){
    	//Parse Data Card Containing List of Files to Collection    	
    	if (GetFileExtension(input)=="dat")
    		this->LoadFromDataCard(input,ext,wRecursive);
    	//Add File to Collection
    	else if ((ext=="")||(GetFileExtension(input)==ext))
    		this->Add(input, false);
    	else
    		return Utility::PrintErrorMessage("Unknown input \""+input+"\". Please use .dat extension for passing data card.");
    }
    else{
	    	std::smatch match;
	    	//Parse Regular Expression
	    	if (std::regex_search (input,match,kRe))    
	    		this->expr = match[1].str();
	    	else
	    		return Utility::PrintErrorMessage("ERROR: File not found "+input);
    }  		
    this->Clean();
    return true;
}

bool TFileCollection::LoadFromDataCard(const std::string input, const std::string ext, const bool wRecursive){
	std::ifstream inFile(input.c_str());	

	//Check If Data Card is valid
	if (!inFile) 
		return Utility::PrintErrorMessage("Cannot open Data Card : "+input);

	std::string line;

	while (std::getline(inFile, line)) 
		this->LoadFromObject(line, ext, wRecursive);

    return true;
}


bool TFileCollection::LoadFromDirectory(const std::string dir, const std::string ext, const bool wRecursive, bool cleaning){
	//Check if 'dir' is a Valid Directory
	if (!IsDirectory(dir))
		return Utility::PrintErrorMessage("ERROR: Invalid Directory "+dir);

	//Create TList containing list of files in the given directory    
	if (this->verbose == 1)
		std::cout<< "Reading Files from the Directory "<<dir<<"\n";
	TSystemDirectory sysdir(dir.c_str(),dir.c_str());
	TList *FileList = sysdir.GetListOfFiles();

	//Check If Directory Contains Files
	if (!FileList)
		return Utility::PrintMessage("WARNING: Nothing Found in Directory "+dir);

	TSystemFile *File;
	TIter Next(FileList);
	while ((File = (TSystemFile*)Next())) {
		//In case the File is a Directory, Decide whether to Add Files Recursively
		if (IsDirectory(File) && wRecursive)
			LoadFromDirectory(File->GetTitle(),ext,wRecursive, cleaning);
		//Add the File to the Collection
		else if ((ext=="")||(GetFileExtension(File->GetName())==ext))
			this->Add(File->GetName(),File->GetTitle(), false);
	}

	if (cleaning)
		this->Clean();
	return true;
}

std::vector<std::string> TFileCollection::GetFileNameGlobs(){
	std::vector<std::string> result;
	for (TSystemFile &f : this->files)
		result.push_back((std::string)f.GetTitle()+"/"+(std::string)f.GetName());
	return result;
}

} // namespace FileManager
} // namespace Alkaid