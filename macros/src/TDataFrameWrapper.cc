// Author: Alkaid Cheng, University of Wisconsin Madison  07/2019

#include "inc/TDataFrameWrapper.h"

TDataFrameWrapper& TDataFrameWrapper::operator=(const TDataFrameWrapper &other){
	lInterface = other.lInterface;
	fInterface = other.fInterface;
	base = other.base;
	InterfaceID = other.InterfaceID;
	return *this;
}

TDataFrameWrapper& TDataFrameWrapper::Define(std::string_view name, std::string_view expression){
	if (InterfaceID == kLInterface)
		lInterface = lInterface.Define(name,expression);
	else if (InterfaceID == kFInterface)
		fInterface = fInterface.Define(name,expression);
	else if (InterfaceID == kBase){
		lInterface = base.Define(name,expression);
		InterfaceID = kLInterface;
	}
	return *this;
}		

TDataFrameWrapper& TDataFrameWrapper::Filter(std::string_view expression, std::string_view name){
	if (InterfaceID == kLInterface)
		fInterface = lInterface.Filter(expression,name);
	else if (InterfaceID == kFInterface)
		fInterface = fInterface.Filter(expression,name);
	else if (InterfaceID == kBase)
		fInterface = base.Filter(expression,name);	
	InterfaceID = kFInterface;
	return *this;
}


TDataFrameWrapper::SnapRet_t TDataFrameWrapper::Snapshot(std::string_view treename, std::string_view filename,
	const std::vector<std::string> & columnList, const bool & lazy ){
	
	ROOT::RDF::RSnapshotOptions opts;
	opts.fLazy = lazy;

	if (InterfaceID == kLInterface)
		return lInterface.Snapshot(treename,filename,columnList,opts);
	else if (InterfaceID == kFInterface)
		return fInterface.Snapshot(treename,filename,columnList,opts);
	else if (InterfaceID == kBase)
		return base.Snapshot(treename,filename,columnList,opts);

	return base.Snapshot(treename,filename,columnList,opts);
}

TDataFrameWrapper::SnapRet_t TDataFrameWrapper::Snapshot(std::string_view treename, std::string_view filename,
	std::string_view columnList , const bool & lazy){
	
	ROOT::RDF::RSnapshotOptions opts;
	opts.fLazy = lazy;

	if (InterfaceID == kLInterface)
		return lInterface.Snapshot(treename,filename,columnList,opts);
	else if (InterfaceID == kFInterface)
		return fInterface.Snapshot(treename,filename,columnList,opts);
	else if (InterfaceID == kBase)
		return base.Snapshot(treename,filename,columnList,opts);

	return base.Snapshot(treename,filename,columnList,opts);
}

std::vector<std::string> TDataFrameWrapper::GetColumnNames(){
	if (InterfaceID == kLInterface)
		return lInterface.GetColumnNames();
	else if (InterfaceID == kFInterface)
		return fInterface.GetColumnNames();
	else if (InterfaceID == kBase)
		return base.GetColumnNames();	
	return std::vector<std::string>{};
}

TDataFrameWrapper::RICutFlowReport TDataFrameWrapper::Report(){
	if (InterfaceID == kLInterface)
		return lInterface.Report();
	else if (InterfaceID == kFInterface)
		return fInterface.Report();
	else if (InterfaceID == kBase)
		return base.Report();		
	return lInterface.Report();
}




TDataFrameWrapper& TDataFrameWrapper::Alias(std::string_view alias, std::string_view columnName){
	if (InterfaceID == kLInterface)
		lInterface = lInterface.Alias(alias,columnName);
	else if (InterfaceID == kFInterface)
		fInterface = fInterface.Alias(alias,columnName);
	else if (InterfaceID == kBase){
		lInterface = base.Alias(alias,columnName);
		InterfaceID = kLInterface;
	}
	return *this;
}

void TDataFrameWrapper::Clear(){
	*this = TDataFrameWrapper();
}

TDataFrameWrapper::RILoopManager TDataFrameWrapper::GetLInterface(){
	if (InterfaceID == kFInterface)
		std::cout<<"WARNING: The extracted interface is not the most recent interface in use! Try using GetFInterface() instead.";
	return lInterface;
}
TDataFrameWrapper::RIJittedFilter TDataFrameWrapper::GetFInterface(){
	if (InterfaceID == kLInterface)
		std::cout<<"WARNING: The extracted interface is not the most recent interface in use! Try using GetLInterface() instead.";
	return fInterface;
}	

ROOT::RDataFrame TDataFrameWrapper::GetBase(){
	return this->base;
}

int TDataFrameWrapper::GetInterfaceID()const{
	return this->InterfaceID;
}