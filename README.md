# Introduction
TRootProcessor is a tool for processing root files. It allows for easy and efficient event selection and variable definition under the framework of RDataFrame from ROOT.

# Setup

## First time setup on lxplus

First git clone the project to a working directory

```
git clone ssh://git@gitlab.cern.ch:7999/clcheng/trootprocessor.git
cd TRootProcessor
```

Setup the environment shell for the most recent root build.
```
source setup.sh
```

Create shared libraries for this tool
```
cd macros
python3.6 MakeShared.py
```

## Normal setup on lxplus
Setup the environment shell for the most recent root build.
```
source setup.sh
```

## Tutorial

### Run an example
You may try to run examples from the directory `Example/`. For example, to run the Hmumu preselection and variable extraction
```
source Example/Hmumu_ATLAS.sh
```

### To run the script
The core script is `TRootProcessor.py`. The most basic usage admits the following format
```
python3.6 TRootProcessor.py -i INPUT_FILES -d DATA_CARD -l LOG_FILE
```

This will perform actions as declared in `DATA_CARD` on the root files defined in `INPUT_FILES`. The name of the processed jobs will be saved to `LOG_FILE` so that you may rerun the script in case of failure while skipping all the jobs that were finished.

### Format of input files
The `INPUT_FILES` accepts the following formats:
1. Name of a root file, e.g. input.root
2. Unix style filename pattern, e.g. /afs/cern.ch/user/s/somebody/input/*.root
3. Numpy array containing an array of root files / Unix style filename pattern, e.g. input.npy
4. (Recommended) Npz file with the following format:
```
'key1' : array([ARRAY_OF_FILE_NAMES_OR_UNIT_STYLE_FILENAME_PATTERN])
'key2' : array([ARRAY_OF_FILE_NAMES_OR_UNIT_STYLE_FILENAME_PATTERN])
.
.
'BasePath' : array([BASE_PATH_TO_THE_FILE_NAMES_DEFINED_ABOVE])
```

Here the `key` will become the file name prefix which you will save your root files. 

For example
```
'/afs/cern.ch/user/s/somebody/processed/ttH' : array(['mc_ttH01.root', 'mc_ttH02.root'])
'/afs/cern.ch/user/s/somebody/processed/ggH' : array(['mc_ggH01.root', 'mc_ggH02.root'])
'BasePath' : array(['/afs/cern.ch/user/s/somebody/input/'])
```

If you are using format 1, 2 or 3. The default key will be empty (i.e. no file name prefix). 

Keep in mind that all the files in the `same key` will be `merged and processed together`.

### Format of data card

The datacard should be a text file (with preferred extension '.dat') containing instructions for what to be done on the input root files.

To add a comment inside the datacard, begin the line with a `#`

NOTE: You can use all the variables that are already defined inside the root files.


#### Defining the Tree to be processed
Begin by defining a the tree name for the root files you are going to process
```
TREENAME: MonoH_Nominal
```

#### Defining a new selection
To define a selection, begin with the keyword `FILTER:`. This will tell the script to define a new selection for the event. 
After the keyword `FILTER:`, put in the selection criteria that is compatible with a c++ boolean format. For example
```
FILTER: (N_BaselineMuons == 0) && (N_SignalMuons==0)
```

#### Defining a new variable
To define a new variable, begin with the keyword `DEFINE:`. This will tell the script to define a new variable for the event. 
After the keyword `DEFINE:`, put in the definition for the new variable that is in c++ compatible format. For example
```
DEFINE: Some_Number = 1
DEFINE: Absolute_CosThetaStar = abs(CosThetaStar)
DEFINE: Electron_P = LorentzVector<PtEtaPhiM4D<Float_t>>( Electron_Pt[0], Electron_Eta[0], Electron_Phi[0], 0.511)
DEFINE: Jet_P = LorentzVector<PtEtaPhiE4D<Float_t>>( Jet_Pt[0], Jet_Eta[0], Jet_Phi[0], Jet_E[0])
DEFINE: Total_P = Electron_P + Jet_P
DEFINE: DiJet_DeltaPhi = DeltaPhi(Jet_Phi[0], Jet_Phi[1])
```

For more complicated definition, you may define a custom definition (c++ compatible) with double curly brackets enclosing the definition. For example
```
DEFINE: DiElectron_E = {{
    auto LeadingElectron_P = LorentzVector<PtEtaPhiM4D<Float_t>>( Electron_Pt[0], Electron_Eta[0], Electron_Phi[0], 0.00511);
    auto SubleadingElectron_P = LorentzVector<PtEtaPhiM4D<Float_t>>( Electron_Pt[1], Electron_Eta[1], Electron_Phi[1], 0.00511);
    auto DiElectron_P = LeadingElectron_P + SubleadingElectron_P;
    return DiElectron_P.E();
}}
```

#### Save and Load Progress using Frame
Sometimes you may want to perform different cuts or define a new variable in different ways depending on the cuts. To do this, you may save the progress of the cuts as well as the definitions that have already been applied and load the progress afterwards by using the keywords `SAVEFRAME:` and  `LOADFRAME:` respectively. For example
```
FILTER: (Muon_Pt > 120.0) && (Muon_Pt < 180.0>)
SAVEFRAME: Base
FILTER: (NJet == 0)
SAVEFRAME: Zero_Jet

LOADFRAME: Base
FILTER: (NJet == 1)
DEFINE: DeltaPhi_J = DeltaPhi(Some_Phi, Jet_Phi[0])
SAVEFRAME: One_Jet

LOADFRAME: Base
FILTER: (NJet == 2)
DEFINE: Dijet_4Momenta = L4V_PtEtaPhiM4D(Jet_Pt[0],Jet_Eta[0],Jet_Phi[0],Jet_M[0]) + L4V_PtEtaPhiM4D(Jet_Pt[1],Jet_Eta[1],Jet_Phi[1],Jet_M[1])
DEFINE: Dijet_E = Dijet_4Momenta.E()
SAVEFRAME: Two_Jet
```

#### Save result as root files or numpy files
You can save the final result to a root file by using the keyword `SAVEASROOT:`. Then you need to define
1. The frame(s) in which the results you want to save.
2. The treename in which the new root file will store the result.
3. The filename(s) of the root file(s) (remember there is a filename prefix defined in your input file keys) .
4. The variables you want to save. To save all variables, write `All`.
5. Whether to convert the root file(s) to numpy file(s). Currently one must save the root file(s) before converting to numpy file(s).

Here is an example
```
SAVE_AS_ROOT: {{
	Frame = [Zero_Jet, One_Jet],
	TreeName = Hmumu,
	FileName = [Hmumu_zero_jet.root, Hmumu_one_jet.root],
	Variables = [Muon_PT, Muon_Eta, Jet_PT],
	ToNumpy = True }}	
```

NOTE: Always use the double curly brackets `{{`, `}}` to enclose the information for saving root file(s). The starting bracket `{{` should always be in the same line as the `SAVEASROOT:` keyword. 

## Custom functions for use
This tool has defined a list of custom functions that you can use in the datacard.

1. `SizeOf(ARRAY)` : to get the size of an array (equivalent to ARRAY.size())<br />
 For example<br />
 Suppose Jet_PT = [12.0, 8.2, 5.3]<br />
 ```
 DEFINE: NJet = SizeOf(Jet_PT)
 ```
 Then NJet = 3<br />

2. `Ascending_Indices(ARRAY)` : to get the indices for sorting array elements in ascending order or their values<br />
 For example, <br />
 Suppose Jet_PT = [12.5, 6.8, 4.2, 7.5]<br />
 ```
 DEFINE: Indices = Ascending_Indices(Jet_PT)
 ```
 Then Indices = [2, 1, 3, 0]<br />

3. `Descending_Indices(ARRAY)` : to get the indices for sorting array elements in descending order or their values<br />
 For example, <br />
 Suppose Jet_PT = [12.5, 6.8, 4.2, 7.5] <br />
 ```
 DEFINE: Indices = Descending_Indices(Jet_PT)
 ```
 Then Indices = [0, 3, 1, 2] <br />
 
4. `Sort_and_Take(ARRAY,INDEX,LEN)`: to get a sorted array sorted according to `INDEX` and keep only the first `LEN` elements<br />
 For example, <br />
 Suppose Jet_PT = [12.5, 6.8, 4.2, 7.5]<br />
 Suppose Jet_Phi = [0.2, 0.5, 0.3, 0.8]<br />
 ```
 DEFINE: new_Jet_PT = Sort_and_Take(Jet_PT,Descending_Indices(Jet_PT),3)
 DEFINE: new_Jet_Phi = Sort_and_Take(Jet_Phi,Descending_Indices(Jet_PT),3)
 ```
 Then new_Jet_PT = [12.5, 7.5, 6.8] <br />
 Then new_Jet_Phi = [0.2, 0.8, 0.5] <br />

5. `MatchAnyInt(ARRAY_OF_INTEGER, TARGET)`: to check if `TARGET` match any of the integers in `ARRAY_OF_INTEGER`<br />
 For example, <br />
 Suppose ID01 = 2<br />
 Suppose ID02 = 5<br />
 ```
 DEFINE: Test1 = 999*MatchAnyInt({1,2,3,4}, ID01)
 DEFINE: Test2 = 999*MatchAnyInt({1,2,3,4}, ID02) 
 ```
 Then Test1 = 999 and Test2 = 0

6. `L4V_PtEtaPhiM(PT,ETA,PHI,M)`: to get the lorentz four vector using particle pt, eta, phi and mass<br />
 For example, <br />
  ```
 DEFINE: Muon_P = L4V_PtEtaPhiM(Muon_PT[0], Muon_Eta[0], Muon_Phi[0], 0.1056)
 ```

7. `L4V_PtEtaPhiE(PT,ETA,PHI,E)`: to get the lorentz four vector using particle pt, eta, phi and energy<br />
 For example, <br />
  ```
 DEFINE: Muon_P = L4V_PtEtaPhiE(Muon_PT[0], Muon_Eta[0], Muon_Phi[0], Muon_E[0])
 ```

8. `L4V_PxPyPzM(PX,PY,PZ,M)`: to get the lorentz four vector using particle px, py, pz and mass<br />
 For example, <br />
  ```
 DEFINE: Muon_P = L4V_PtEtaPhiE(Muon_Px[0], Muon_Py[0], Muon_Pz[0], 0.1056)
 ``` 

9. `CosineThetaStar(LORENTZVECTOR_1,LORENTZVECTOR_2)`: to get the Cosine Theta Star of the two Lorentz vectors `LORENTZVECTOR_1` and `LORENTZVECTOR_2`<br />
 For example, <br />
 ```
 DEFINE: Muon_P1 = L4V_PtEtaPhiM(Muon_PT[0], Muon_Eta[0], Muon_Phi[0], 0.1056)
 DEFINE: Muon_P2 = L4V_PtEtaPhiM(Muon_PT[1], Muon_Eta[1], Muon_Phi[1], 0.1056)
 DEFINE: Dimuon_CosThetaStar = CosineThetaStar(Muon_P1, Muon_P2)
 ```

10. `DescendingSortByColumn(ARRAY2D, COLUMN)`: sort a 2D vector in descending order by the value in the `COLUMN`-th column<br />
 For example, <br />
 Suppose Muon_PT = [1,2,3], Muon_Eta = [3,2,1], Muon_Phi = [2,1,3] <br />
 Such that v2D = [[1,2,3], [3,2,1], [2,1,3]]
 ```
 DEFINE: test = {{
     auto v2D = RVec<RVec<Float_t>>{Muon_PT, Muon_Eta, Muon_Phi};
     DescendingSortByColumn(v2D, 0);
     return v2D;
 }}
 ```
 Then test = [[3,2,1], [2,1,3], [1,2,3]]

11. `AscendingSortByColumn(ARRAY2D, COLUMN)`: sort a 2D vector in ascending order by the value in the `COLUMN`-th column<br />
 For example, <br />
 Suppose Muon_PT = [1,2,3], Muon_Eta = [3,2,1], Muon_Phi = [2,1,3] <br />
 Such that v2D = [[1,2,3], [3,2,1], [2,1,3]]
 ```
 DEFINE: test = {{
     auto v2D = RVec<RVec<Float>>{Muon_PT, Muon_Eta, Muon_Phi};
     AscendingSortByColumn(v2D, 0);
     return v2D;
 }}
 ```
 Then test = [[1,2,3], [2,1,3], [3,2,1]]

12. `GetEFromPtEtaPhiM(PT_ARRAY, ETA_ARRAY, PHI_ARRAY, M_ARRAY)`: get the array of energy from arrays of pt, eta, phi and mass (you can also put in a scalar for mass)<br />
 For example, 
 ```
 DEFINE: Jet_E = GetEFromPtEtaPhiM( Jet_PT, Jet_Eta, Jet_Phi, Jet_M)
 ```

13. `GetMFromPtEtaPhiE(PT_ARRAY, ETA_ARRAY, PHI_ARRAY, E_ARRAY)`: get the array of mass from arrays of pt, eta, phi and energy<br />
 For example,
 ```
 DEFINE: Jet_M = GetMFromPtEtaPhiE( Jet_PT, Jet_Eta, Jet_Phi, Jet_E)
 ```
14. `Transpose(ARRAY2D)`: get the transpose of a 2D array<br />
 For example, <br />
 Suppose v2D = [[1,2,3], [3,2,1], [2,1,3]]
 ```
 DEFINE: transposed_v2D = Transpose(v2D)
 ```
 Then transposed_v2D = [[1,3,2], [2,2,1], [3,1,3]]


