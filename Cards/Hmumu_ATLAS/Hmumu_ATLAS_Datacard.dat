# Specify the tree name in the root files you are going to process
TREE_NAME: DiMuonNtuple

######################################################
#				     Pre-Selection                   #
######################################################
FILTER: (Muons_Minv_MuMu_Fsr >= 110.00) && (Muons_Minv_MuMu_Fsr <= 180.00)
FILTER: Muons_Multip == 2
FILTER: Event_HasBJet == 0

# Define the variables common to all categories
DEFINE: vDimuon_PT = Z_PT_FSR
DEFINE: vDimuon_Y = Z_Y_FSR
DEFINE: vDimuon_CosThetaStar = abs(Muons_CosThetaStar)

# Write your custom (multiple lines) definition inside a double-braces
DEFINE: vWeight = {{
	auto Channel_1 = (Int_t) MatchAnyInt({364100, 364101, 364103, 364104, 364106},EventInfo_ChannelNumber);
	auto Channel_2 = (Int_t) MatchAnyInt({366300, 366301, 366303, 366304, 366306},EventInfo_ChannelNumber);
	return GlobalWeight*(1-(Channel_1*(Truth_Boson_Mass >= 10000.00)) - (Channel_2*(Truth_Boson_Mass <= 10000.00)));}}
DEFINE: NJet = SizeOf(Jets_PT)

SAVE_FRAME: preselected

######################################################
#				    Zero Jet Events                  #
######################################################

FILTER: NJet == 0
SAVE_FRAME: zero_jet

######################################################
#				     One Jet Events                  #
######################################################

LOAD_FRAME: preselected
FILTER: NJet == 1
FILTER: All(Jets_PassFJVT==1)
DEFINE: vJets_PT = Jets_PT
DEFINE: vJets_Eta = Jets_Eta
DEFINE: vJets_DeltaPhi_mumuj = DeltaPhi(Jets_Phi,Z_Phi_FSR)
SAVE_FRAME: one_jet

######################################################
#				     Two Jet Events                  #
######################################################

LOAD_FRAME: preselected
FILTER: NJet >= 2
FILTER: All(Jets_PassFJVT==1)
DEFINE: DeltaPhi_mumuj = DeltaPhi(Jets_Phi,Z_Phi_FSR)

#Sort the jets in ascending order of their pt and update the related variables
DEFINE: JetIndex = Ascending_Indices(Jets_PT)
##################################################################
## The Sort_and_Take(VAR_NAME,INDEX,N) function will
## 1. Sort an array according to the given index and then 
## 2. take the first N values in the array
##################################################################
DEFINE: vJets_PT = Sort_and_Take(Jets_PT,JetIndex,2)
DEFINE: vJets_Eta = Sort_and_Take(Jets_Eta,JetIndex,2)
DEFINE: vJets_Phi = Sort_and_Take(Jets_Phi,JetIndex,2)
DEFINE: vJets_E = Sort_and_Take(Jets_E,JetIndex,2)
DEFINE: vJets_DeltaPhi_mumuj = Sort_and_Take(DeltaPhi_mumuj,JetIndex,2)

#Define a 4 vector for the two jets and add them together 
DEFINE: J1J2 = L4V_PtEtaPhiE(vJets_PT[0],vJets_Eta[0],vJets_Phi[0],vJets_E[0]) + L4V_PtEtaPhiE(vJets_PT[1],vJets_Eta[1],vJets_Phi[1],vJets_E[1])

#Define the dijet variables
DEFINE: vDijet_PT = J1J2.Pt()
DEFINE: vDijet_Y = J1J2.Rapidity()
DEFINE: vDijet_Minv = J1J2.M()
DEFINE: vDijet_DeltaPhi = DeltaPhi(J1J2.Phi(),Z_Phi_FSR)
SAVE_FRAME: two_jet

######################################################
#				       Save Events                   #
######################################################

SAVE_AS_ROOT:{{
	Frame = [zero_jet],
	TreeName = DiMuonNtuple,
	FileName = [zero_jet.root],
	Variables = [vDimuon_PT,vDimuon_Y,vDimuon_CosThetaStar,vWeight],
	ToNumpy = True}}

SAVE_AS_ROOT:{{
	Frame = [one_jet],
	TreeName = DiMuonNtuple,
	FileName = [one_jet.root], 
	Variables = [vDimuon_PT,vDimuon_Y,vDimuon_CosThetaStar,vJets_PT,vJets_Eta,vJets_DeltaPhi_mumuj,vWeight],
	ToNumpy = True}}

SAVE_AS_ROOT:{{
	Frame = [two_jet],
	TreeName = DiMuonNtuple,
	FileName = [two_jet.root],
	Variables = [vDimuon_PT,vDimuon_Y,vDimuon_CosThetaStar,vJets_PT,vJets_Eta,vJets_DeltaPhi_mumuj,vDijet_PT,vDijet_Y,vDijet_Minv,vDijet_DeltaPhi,vWeight],
	ToNumpy = True}}
