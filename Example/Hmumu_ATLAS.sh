# Obtain the path to the python script
path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
path=$( echo ${path} | sed -e 's/\(Example\)*$//g')
# Here is the actual command
time python3.6 ${path}/TRootProcessor.py -i ${path}/Cards/Hmumu_ATLAS/Hmumu_ATLAS_v20_Hmumu_Input.npz -d ${path}/Cards/Hmumu_ATLAS/Hmumu_ATLAS_Datacard.dat -l ${path}/Logs/Hmumu_ATLAS_finished_keys.txt