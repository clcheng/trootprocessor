# Obtain the path to the python script
path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
path=${path%Example}
# Here is the actual command
delphes_path=/eos/atlas/unpledged/group-wisc/users/Alkaid/local/Delphes/lib
time python3.6 ${path}/TRootProcessor.py -i ${path}/Cards/Hmumu_Delphes/Hmumu_Delphes_Input.npz -d ${path}/Cards/Hmumu_Delphes/Hmumu_Delphes_DataCard.dat -l ${path}/Logs/Hmumu_Delphes_finished_keys.txt --delphes=${delphes_path}