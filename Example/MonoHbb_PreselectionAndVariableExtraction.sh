# Obtain the path to the python script
path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
path=$( echo ${path} | sed -e 's/\(Example\)*$//g')
# Here is the actual command
time python3.6 ${path}/TRootProcessor.py --input=${path}/Cards/MonoHbb/MonoHbb_PreselectionAndVariableExtraction_InputFiles.npz  --datacard=${path}/Cards/MonoHbb/MonoHbb_PreselectionAndVariableExtraction_DataCard.dat --log=${path}/Logs/MonoHbb_PreselectionAndVariableExtraction_Finished_Keys.txt